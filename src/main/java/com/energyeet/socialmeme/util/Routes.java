package com.energyeet.socialmeme.util;

/**
 * Interface for all the routes
 */
public interface Routes {

    String USER_ROUTE = "/user";
    String USER_REGISTER_ROUTE = "/registration";
    String USER_LOGIN_ROUTE = "/login";
    String USER_LOGOUT_ROUTE = "/logout";
    String USER_GET_PROFILE = "/profile";
    String USER_UPDATE_PROFILE_PICTURE = "/profile/picture";
    String USER_SEARCH_PROFILE = "/search";
    String USER_PROFILE_PICTURE = "/profile/get/picture";

    String FOLLOW_ROUTE = "/follow";
    String FOLLOW_USER_ROUTE = "/user";

    String POST_ROUTE = "/post";
    String POST_CREATE_ROUTE = "/create";
    String POST_GET_ALL_USER_POST_ROUTE = "/all/user";
    String POST_GET_NEWEST_FROM_FOLLOWED = "/followed/all";
    String POST_LIKE_ROUTE = "/like";
    String POST_DELETE_ROUTE = "/delete";
    String POST_GET_BY_HEADING = "/all/heading";
    String POST_GET_BY_CATEGORY = "/all/category";

    String COMMENT_ROUTE = "/comment";
    String COMMENT_ADD_ROUTE = "/add";

    String CATEGORY_ROUTE = "/category";
    String CATEGORY_CREATE_ROUTE = "/create";
    String CATEGORY_GET_ALL_ROUTE = "/get/all";
}
