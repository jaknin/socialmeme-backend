package com.energyeet.socialmeme.util;

import com.energyeet.socialmeme.data.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Class for inserting logged in users
 */
@Component
public class LoginPool {

    /**
     * User pool
     */
    private List<String> users;

    public LoginPool() {
        this.users = new ArrayList<>();
    }

    /**
     * Inserts user into pool
     * @param user {@link User} user to insert
     * @return {@link String} the token
     */
    public String insertUser(User user) {
        this.users.add(hashUser(user));
        return hashUser(user);
    }

    /**
     * Checks if user is in the pool, therefore if it is authenticated
     * @param user {@link String} user to check
     * @return {@link boolean} if the user is authenticated
     */
    public boolean checkUser(String user) {
        return this.users.contains(user);
    }

    /**
     * Remove User from the pool, therefore he isn't authenticated anymore
     * @param user {@link User} user to remove
     */
    public void removeUser(User user) {
        users.remove(hashUser(user));
    }

    /**
     * Hashes password and username from user
     * @param user {@link User} user to hash
     * @return {@link String} hashed user password and username
     */
    private String hashUser(User user) {
        return Hasher.hash(user.getUserName() + user.getPassword());
    }
}
