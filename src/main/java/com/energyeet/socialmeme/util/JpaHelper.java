package com.energyeet.socialmeme.util;

import com.energyeet.socialmeme.SocialMemeConstants;
import com.energyeet.socialmeme.data.*;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Component
public class JpaHelper implements SocialMemeConstants {

    private EntityManagerFactory emFactory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT);
    private EntityManager entityManager = emFactory.createEntityManager();

    /**
     * Save new User to the database
     * @param user {@link User} user to save
     */
    public void save(User user) {
        user.setFkPicture(1);
        entityManager.getTransaction().begin();
        entityManager.persist(user);
        entityManager.getTransaction().commit();
    }

    /**
     * Get User from database by username
     * @param username {@link String} username to search for
     * @return {@link User} user
     */
    public User findByUsername(String username) {
        TypedQuery<User> query = entityManager.createQuery("SELECT u FROM User u WHERE u.userName = :username", User.class);
        query.setParameter("username", username);
        return query.getSingleResult();
    }

    /**
     * Get user from database by username and password
     * @param username {@link String} username of the user
     * @param password {@link String} password of the user
     * @return {@link boolean} if user was found
     */
    public boolean findByUserNameAndPassword(String username, String password) {
        TypedQuery<User> query = entityManager.createQuery("SELECT u FROM User u WHERE u.userName = :username AND u.password = :password", User.class);
        query.setParameter("username", username);
        query.setParameter("password", password);
        try {
            return query.getSingleResult() != null;
        } catch (NoResultException ex) {
            return false;
        }
    }

    /**
     * Get user by Id
     * @param id {@link Integer} id
     * @return {@link User}
     */
    public User findUserById(int id) {
        TypedQuery<User> query = entityManager.createQuery("SELECT u FROM User u WHERE u.pkId = :id", User.class);
        query.setParameter("id", id);
        return query.getSingleResult();
    }

    /**
     * Follow a user
     * @param userFollows {@link Integer} the user who follows someone
     * @param userToFollow {@link Integer} the user that is being followed
     */
    public void followUser(int userFollows, int userToFollow) {
        Follow follow = new Follow();
        follow.setFkFollows(userFollows);
        follow.setFkIsFollowed(userToFollow);

        entityManager.getTransaction().begin();
        entityManager.persist(follow);
        entityManager.getTransaction().commit();
    }

    /**
     * Add a picture to the database
     * @param pictureCode {@link String} picture encoded in Base64
     * @return {@link Integer} id of the picture
     */
    public int addPicture(String pictureCode) {
        Picture picture = new Picture();
        picture.setPicture(pictureCode);

        entityManager.getTransaction().begin();
        entityManager.persist(picture);
        entityManager.getTransaction().commit();
        picture = null;

        //Select the picture from the database and return the id from it
        TypedQuery<Integer> query = entityManager.createQuery("SELECT p.pkId FROM Picture p WHERE p.picture = :string", Integer.class);
        query.setParameter("string", pictureCode);
        List<Integer> list = query.getResultList();
        return list.get(list.size() - 1);
    }

    /**
     * Add new Post to the database
     * @param post {@link Post} post to add
     */
    public void addPost(Post post) {
        entityManager.getTransaction().begin();
        entityManager.persist(post);
        entityManager.getTransaction().commit();
    }

    /**
     * Get all posts from user
     * @param userId {@link Integer} user id
     * @return {@link List} of all posts
     */
    public List<Post> getAllPostsFromUser(int userId) {
        TypedQuery<Post> query = entityManager.createQuery("SELECT p FROM Post p WHERE p.fkUser = :userId", Post.class);
        query.setParameter("userId", userId);
        return query.getResultList();
    }

    /**
     * Get Picture by Id
     * @param pictureId {@link Integer} id of the picture
     * @return {@link Picture}
     */
    public Picture getPictureById(int pictureId) {
        TypedQuery<Picture> query = entityManager.createQuery("SELECT p FROM Picture p WHERE p.pkId = :pictureId", Picture.class);
        query.setParameter("pictureId", pictureId);
        return query.getSingleResult();
    }

    /**
     * Get Picture by its COde
     * @param pictureCode {@link String} picture code
     * @return {@link Picture}
     */
    public Picture getPictureByCode(String pictureCode) {
        TypedQuery<Picture> query = entityManager.createQuery("SELECT p FROM Picture p WHERE p.picture = :pictureCode", Picture.class);
        query.setParameter("pictureCode", pictureCode);
        return query.getSingleResult();
    }

    /**
     * post a comment
     * @param post {@link Integer} id of the post
     * @param user {@link Integer} id of the user who comments
     * @param text {@link String} text from comment
     */
    public void postComment(int post, int user, String text) {
        Comment comment = new Comment();
        comment.setFkPost(post);
        comment.setFkUser(user);
        comment.setComment(text);

        entityManager.getTransaction().begin();
        entityManager.persist(comment);
        entityManager.getTransaction().commit();
    }

    /**
     * Like a post
     * @param post {@link com.energyeet.socialmeme.data.dto.Post} post to like
     */
    public void likePost(com.energyeet.socialmeme.data.dto.Post post) {
        TypedQuery<Post> query = entityManager.createQuery("SELECT p FROM Post p WHERE p.pkId = :postId", Post.class);
        query.setParameter("postId", post.getId());
        Post post1 = query.getSingleResult();
        post1.setLikes(post1.getLikes() + 1);
        entityManager.getTransaction().begin();
        entityManager.persist(post1);
        entityManager.getTransaction().commit();
    }

    /**
     * Write new Category into the database
     * @param category {@link Category} category to write
     */
    public void createCategory(Category category) {
        entityManager.getTransaction().begin();
        entityManager.persist(category);
        entityManager.getTransaction().commit();
    }

    /**
     * Get all categories from the database
     * @return {@link List} of {@link Category}
     */
    public List<Category> getAllCategories() {
        TypedQuery<Category> query = entityManager.createQuery("SELECT c FROM Category c", Category.class);
        return query.getResultList();
    }

    /**
     * Method for deleting a post from the database
     * @param post {@link com.energyeet.socialmeme.data.dto.Post} id of the post that has to be deleted
     * @return {@link Integer} amount of records deleted. Should be 1 as you can only delete one post at the time
     */
    public int deletePost(com.energyeet.socialmeme.data.dto.Post post) {
        int deletionCount = 0; // At the end of the method it should be 2
        TypedQuery<Integer> query = entityManager.createQuery("DELETE FROM Post p WHERE p.pkId = :postId", Integer.class);
        query.setParameter("postId", post.getId());
        TypedQuery<Integer> query1 = entityManager.createQuery("DELETE FROM Picture p WHERE p.picture = :picture", Integer.class);
        query1.setParameter("picture", post.getPicture());
        deletionCount += query1.executeUpdate();
        deletionCount += query.executeUpdate();
        return deletionCount;
    }

    /**
     * Method for deleting a comment from the database
     * @param commentId {@link Integer} id of the comment that has to be deleted
     * @return {@link Integer} amount of records delete. Should be 1 as you can only delete one comment at the time
     */
    public int deleteComment(int commentId) {
        TypedQuery<Integer> query = entityManager.createQuery("DELETE FROM Comment c WHERE c.pkId = :commentId", Integer.class);
        query.setParameter("commentId", commentId);
        return query.executeUpdate();
    }

    public List<Follow> getAllFollowedUsersFromUser(User user) {
        TypedQuery<Follow> query = entityManager.createQuery("SELECT f FROM Follow f WHERE f.fkFollows = :userId", Follow.class);
        query.setParameter("userId", user.getPkId());
        return query.getResultList();
    }

    /**
     * Get the amount of followers from a user
     * @param user {@link User}
     * @return {@link Integer} amount of followers
     */
    public Long getAmountOfFollowers(User user) {
        TypedQuery<Long> query = entityManager.createQuery("SELECT COUNT(f) FROM Follow f WHERE f.fkIsFollowed = :userId", Long.class);
        query.setParameter("userId", user.getPkId());
        return query.getSingleResult();
    }

    /**
     * Get the amount of followed users from a user
     * @param user {@link User}
     * @return {@link Integer} amount of followed users
     */
    public Long getAmountOfFollowed(User user) {
        TypedQuery<Long> query = entityManager.createQuery("SELECT COUNT(f) FROM Follow f WHERE f.fkFollows = :userId", Long.class);
        query.setParameter("userId", user.getPkId());
        return query.getSingleResult();
    }

    /**
     * Get posts that match the heading
     * @param heading {@link String} heading of the post
     * @return {@link List} of {@link com.energyeet.socialmeme.data.dto.Post}
     */
    public List<com.energyeet.socialmeme.data.dto.Post> getPostByHeading(String heading) {
        List<Post> dataPosts;
        List<com.energyeet.socialmeme.data.dto.Post> posts = new ArrayList<>();
        heading = new StringBuilder().append("%").append(heading).append("%").toString();
        TypedQuery<Post> query = entityManager.createQuery("SELECT p FROM Post p WHERE p.heading LIKE :heading", Post.class);
        query.setParameter("heading", heading);
        dataPosts = query.getResultList();
        dataPosts.forEach(element -> {
            com.energyeet.socialmeme.data.dto.Post post = new com.energyeet.socialmeme.data.dto.Post(element.getPkId(), getPictureById(element.getFkPicture()).getPicture(), findUserById(element.getFkUser()).getUserName(), element.getLikes(), element.getTimestamp(), element.getHeading(), "");
            posts.add(post);
        });
        return posts;
    }

    /**
     * Get users by username
     * @param username {@link String} username of users to search for
     * @return {@link List} of {@link User}
     */
    public List<User> getUsersByName(String username) {
        username = new StringBuilder().append("%").append(username).append("%").toString();
        TypedQuery<User> query = entityManager.createQuery("SELECT u FROM User u WHERE u.userName LIKE :username", User.class);
        query.setParameter("username", username);
        return query.getResultList();
    }

    /**
     * Update Profile picture
     * @param pictureId {@link Integer} primary key of the picture
     * @param user {@link User} user to update the profile pciture from
     */
    public void updateProfilePicture(int pictureId, User user) {
        entityManager.getTransaction().begin();
        user.setFkPicture(pictureId);
        entityManager.persist(user);
        entityManager.getTransaction().commit();
    }

    /**
     * Get Post by picture and user
     * @param fkPicture {@link Integer} picture id
     * @param fkUser {@link Integer} user id
     * @return {@link Post}
     */
    public Post getPost(int fkPicture, int fkUser) {
        TypedQuery<Post> query = entityManager.createQuery("SELECT p FROM Post p WHERE p.fkUser = :fkUser AND p.fkPicture = :fkPicture", Post.class);
        query.setParameter("fkUser", fkUser);
        query.setParameter("fkPicture", fkPicture);
        return query.getSingleResult();
    }

    /**
     * Add category to the database
     * @param category {@link Category}
     */
    public void addCategory(Category category) {
        entityManager.getTransaction().begin();
        entityManager.persist(category);
        entityManager.getTransaction().commit();
    }

    /**
     * Add connection of category to post into the database
     * @param categoryPost {@link CategoryPost}
     */
    public void addCategoryPost(CategoryPost categoryPost) {
        entityManager.getTransaction().begin();
        entityManager.persist(categoryPost);
        entityManager.getTransaction().commit();
    }

    /**
     * Get Category
     * @param category {@link Category} category to check
     * @return {@link Category}
     */
    public Category getCategoryByName(Category category) {
        TypedQuery<Category> query = entityManager.createQuery("SELECT c FROM Category c WHERE c.category = :categoryName", Category.class);
        query.setParameter("categoryName", category.getCategory());
        return query.getSingleResult();
    }

    /**
     * Get posts by category
     * @param category {@link String} name of the category
     * @return {@link List} of {@link com.energyeet.socialmeme.data.dto.Post}
     */
    public List<com.energyeet.socialmeme.data.dto.Post> getPostsByCategory(String category) {
        Category categoryObj = new Category();
        categoryObj.setCategory(category);
        categoryObj = this.getCategoryByName(categoryObj);
        TypedQuery<Post> query = entityManager.createQuery("SELECT p FROM CategoryPost cp INNER JOIN Post p ON cp.fkPost = p.pkId WHERE cp.fkCategory = :fkCategory", Post.class);
        query.setParameter("fkCategory", categoryObj.getPkId());
        List<Post> posts = query.getResultList();
        List<com.energyeet.socialmeme.data.dto.Post> newPosts = new ArrayList<>();

        posts.forEach(element -> {
            com.energyeet.socialmeme.data.dto.Post post = new com.energyeet.socialmeme.data.dto.Post(element.getPkId(), this.getPictureById(element.getFkPicture()).getPicture(), findUserById(element.getFkUser()).getUserName(), element.getLikes(), element.getTimestamp(), element.getHeading(), "");
            newPosts.add(post);
        });
        return newPosts;
    }
}
