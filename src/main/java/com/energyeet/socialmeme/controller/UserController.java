package com.energyeet.socialmeme.controller;

import com.energyeet.socialmeme.data.Picture;
import com.energyeet.socialmeme.data.User;
import com.energyeet.socialmeme.data.dto.Post;
import com.energyeet.socialmeme.service.LoginPoolService;
import com.energyeet.socialmeme.service.PictureService;
import com.energyeet.socialmeme.service.PostService;
import com.energyeet.socialmeme.service.UserService;
import com.energyeet.socialmeme.util.Routes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;

/**
 * Controller for the user related stuff
 */
@RestController
@RequestMapping(value = Routes.USER_ROUTE)
public class UserController implements Routes {

    @Autowired
    private UserService userService;
    @Autowired
    private LoginPoolService loginPoolService;
    @Autowired
    private PictureService pictureService;
    @Autowired
    private PostService postService;

    @CrossOrigin(origins = "*")
    @PostMapping(value = USER_REGISTER_ROUTE)
    public ResponseEntity registration(@RequestBody User user) {
        try {
            userService.save(user);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @CrossOrigin(origins = "*")
    @PostMapping(value = USER_LOGOUT_ROUTE)
    public ResponseEntity logout(@RequestBody User user) {
        try {
            this.loginPoolService.deauthenticateUser(user);
            return ResponseEntity.status(HttpStatus.OK).build();
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @CrossOrigin(origins = "*")
    @PostMapping(value = USER_LOGIN_ROUTE)
    public ResponseEntity login(@RequestBody User user) {
        if (userService.findByUserNameAndPassword(user.getUserName(), user.getPassword())) {
            String code  = loginPoolService.authenticateUser(user);
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("Pragma", code);
            return ResponseEntity.status(HttpStatus.OK).headers(httpHeaders).build();

        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = USER_GET_PROFILE, produces = "application/json", params = {"username"})
    public ResponseEntity getProfile(@RequestParam("username") String username, HttpServletRequest request) {
        String code = request.getHeader("Set-Cookie");
        if (loginPoolService.isUserAuthenticated(code)) {
            User user = userService.findByUsername(username);
            Picture picture = pictureService.getPictureById(user.getFkPicture());
            List<Post> posts = postService.getAllPostsFromUser(user.getUserName());
            Collections.sort(posts);
            long follower = userService.getAmountOfFollowers(user);
            long followed = userService.getAmountOfFollowed(user);
            com.energyeet.socialmeme.data.dto.User finalUser = new com.energyeet.socialmeme.data.dto.User(username, follower, followed, picture.getPicture(), posts);
            return ResponseEntity.status(HttpStatus.OK).body(finalUser);
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    @CrossOrigin(origins = "*")
    @PutMapping(value = USER_UPDATE_PROFILE_PICTURE, consumes = "application/json", params = {"username"})
    public ResponseEntity updateProfilePicture(@RequestBody Picture picture, @RequestParam("username") String username, HttpServletRequest request) {
        String code = request.getHeader("Set-Cookie");
        if (loginPoolService.isUserAuthenticated(code)) {
            this.pictureService.addPicture(picture.getPicture());
            int pictureId = this.pictureService.getPictureByCode(picture.getPicture()).getPkId();
            User user = this.userService.findByUsername(username);
            this.userService.updateProfilePicture(pictureId, user);
            return ResponseEntity.status(HttpStatus.OK).build();
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = USER_SEARCH_PROFILE, produces = "application/json", params = {"username"})
    public ResponseEntity getUsersByName(@RequestParam("username") String username, HttpServletRequest request) {
        String code = request.getHeader("Set-Cookie");
        if (loginPoolService.isUserAuthenticated(code)) {
            return ResponseEntity.status(HttpStatus.OK).body(this.userService.getUsersByName(username));
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    @CrossOrigin("*")
    @GetMapping(value = USER_PROFILE_PICTURE, produces = "application/json", params = {"username"})
    public ResponseEntity getUserProfilePicture(@RequestParam("username") String username, HttpServletRequest request) {
        String code = request.getHeader("Set-Cookie");
        if (loginPoolService.isUserAuthenticated(code)) {
            User user = this.userService.findByUsername(username);
            Picture picture = this.pictureService.getPictureById(user.getFkPicture());
            return ResponseEntity.status(HttpStatus.OK).body(picture);
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }
}
