package com.energyeet.socialmeme.controller;

import com.energyeet.socialmeme.data.dto.CommentDto;
import com.energyeet.socialmeme.service.CommentService;
import com.energyeet.socialmeme.service.LoginPoolService;
import com.energyeet.socialmeme.util.Routes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 *Controller for the/comment path
 */
@RestController

@RequestMapping(value = Routes.COMMENT_ROUTE)
public class CommentController implements Routes {

    @Autowired
    private LoginPoolService loginPoolService;
    @Autowired
    private CommentService commentService;

    @CrossOrigin(origins = "*")
    @PostMapping(value = COMMENT_ADD_ROUTE, consumes = "application/json")
    public ResponseEntity addComment(@RequestBody CommentDto comment, HttpServletRequest request) {
        String code = request.getHeader("Set-Cookie");
        if (loginPoolService.isUserAuthenticated(code)) {
            commentService.postComment(comment.getPostId(), comment.getUsername(), comment.getText());
            return ResponseEntity.status(HttpStatus.OK).build();
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }
}
