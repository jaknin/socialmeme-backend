package com.energyeet.socialmeme.controller;

import com.energyeet.socialmeme.service.FollowService;
import com.energyeet.socialmeme.service.LoginPoolService;
import com.energyeet.socialmeme.util.Routes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Controller for all the following user matters
 */
@RestController
@RequestMapping(value = Routes.FOLLOW_ROUTE)
public class FollowController implements Routes {

    @Autowired
    private FollowService followService;
    @Autowired
    private LoginPoolService loginPoolService;

    @CrossOrigin(origins = "*")
    @GetMapping(value = FOLLOW_USER_ROUTE, params = {"username", "username1"})
    public ResponseEntity followUser(@RequestParam(value = "username") String username, @RequestParam(value = "username1") String username1, HttpServletRequest request) {
        String code = request.getHeader("Set-Cookie");
        if (loginPoolService.isUserAuthenticated(code)) {
            followService.followUser(username, username1);
            return ResponseEntity.status(HttpStatus.OK).build();
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }
}
