package com.energyeet.socialmeme.controller;

import com.energyeet.socialmeme.data.Category;
import com.energyeet.socialmeme.service.CategoryService;
import com.energyeet.socialmeme.service.LoginPoolService;
import com.energyeet.socialmeme.util.Routes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Controller for categories
 */
@RestController
@RequestMapping(value = Routes.CATEGORY_ROUTE)
public class CategoryController implements Routes {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private LoginPoolService loginPoolService;

    @CrossOrigin(origins = "*")
    @PostMapping(value = CATEGORY_CREATE_ROUTE, consumes = "application/json")
    public ResponseEntity createCategory(@RequestBody Category category, HttpServletRequest request) {
        String code = request.getHeader("Set-Cookie");
        if (loginPoolService.isUserAuthenticated(code)) {
            categoryService.createCategory(category);
            return ResponseEntity.status(HttpStatus.OK).build();
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = CATEGORY_GET_ALL_ROUTE, produces = "application/json")
    public ResponseEntity getAllCategories(HttpServletRequest request) {
        String code = request.getHeader("Set-Cookie");
        if (loginPoolService.isUserAuthenticated(code)) {
            return ResponseEntity.status(HttpStatus.OK).body(categoryService.getAllCategories());
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }
}
