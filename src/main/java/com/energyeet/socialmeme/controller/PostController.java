package com.energyeet.socialmeme.controller;

import com.energyeet.socialmeme.SocialMemeConstants;
import com.energyeet.socialmeme.data.User;
import com.energyeet.socialmeme.data.dto.Post;
import com.energyeet.socialmeme.service.FollowService;
import com.energyeet.socialmeme.service.LoginPoolService;
import com.energyeet.socialmeme.service.PostService;
import com.energyeet.socialmeme.service.UserService;
import com.energyeet.socialmeme.util.Routes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Controller for post
 */
@RestController
@RequestMapping(value = Routes.POST_ROUTE)
public class PostController implements Routes, SocialMemeConstants {

    @Autowired
    private LoginPoolService loginPoolService;

    @Autowired
    private PostService postService;

    @Autowired
    private UserService userService;

    @Autowired
    private FollowService followService;

    /**
     * Create new Post
     * @param post {@link Post} post to add
     * @param request {@link HttpServletRequest} request
     * @return {@link ResponseEntity}
     */
    @CrossOrigin(origins = "*")
    @PostMapping(value = POST_CREATE_ROUTE, consumes = "application/json")
    public ResponseEntity createPost(@RequestBody Post post, HttpServletRequest request) {
        String code = request.getHeader("Set-Cookie");
        if (loginPoolService.isUserAuthenticated(code)) {
            postService.addPost(post);
            return ResponseEntity.status(HttpStatus.OK).build();
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    /**
     * Get all posts from a user
     * @param username {@link String} username of the user to get the posts from
     * @param request {@link HttpServletRequest} request
     * @return {@link ResponseEntity} with the posts
     */
    @CrossOrigin(origins = "*")
    @GetMapping(value = POST_GET_ALL_USER_POST_ROUTE, params = {"username"})
    public ResponseEntity getAllPostsFromUser(@RequestParam("username") String username, HttpServletRequest request) {
        String code = request.getHeader("Set-Cookie");
        if (loginPoolService.isUserAuthenticated(code)) {
            List<Post> posts = postService.getAllPostsFromUser(username);
            Collections.sort(posts);
            return ResponseEntity.status(HttpStatus.OK).body(posts);
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    /**
     * Like a post
     * @param post {@link Post} post to like
     * @param request {@link HttpServletRequest} request
     * @return {@link ResponseEntity} with the request status
     */
    @CrossOrigin(origins = "*")
    @PostMapping(value = POST_LIKE_ROUTE, consumes = "application/json")
    public ResponseEntity likePost(@RequestBody Post post, HttpServletRequest request) {
        String code = request.getHeader("Set-Cookie");
        if (loginPoolService.isUserAuthenticated(code)) {
            postService.likePost(post);
            return ResponseEntity.status(HttpStatus.OK).build();
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    @CrossOrigin(origins = "*")
    @DeleteMapping(value = POST_DELETE_ROUTE, consumes = "application/json")
    public ResponseEntity deletePost(@RequestBody Post post, HttpServletRequest request) {
        String code = request.getHeader("Set-Cookie");
        if (loginPoolService.isUserAuthenticated(code)) {
            int amount = postService.deletePost(post);
            if (amount != 2) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
            }
            return ResponseEntity.status(HttpStatus.OK).build();
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    @CrossOrigin(origins = "*")
    @GetMapping(value = POST_GET_NEWEST_FROM_FOLLOWED, produces = "application/json", params = {"username"})
    public ResponseEntity getNewestPostsFromFollowedUsers(@RequestParam("username") String username, HttpServletRequest request) {
        String code = request.getHeader("Set-Cookie");
        if (loginPoolService.isUserAuthenticated(code)) {
            User user = userService.findByUsername(username);
            List<User> followedUsers = followService.getAllFollowedUsers(user);
            List<Post> posts = postService.getAllPostsFromFollowedUsers(followedUsers);
            Collections.sort(posts);
            return ResponseEntity.status(HttpStatus.OK).body(posts);
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    @CrossOrigin("*")
    @GetMapping(value = POST_GET_BY_HEADING, produces = "application/json", params = {"heading"})
    public ResponseEntity getPostByHeading(@RequestParam("heading") String heading, HttpServletRequest request) {
        String code = request.getHeader("Set-Cookie");
        if (loginPoolService.isUserAuthenticated(code)) {
            List<Post> posts = this.postService.getPostsByHeading(heading);
            return ResponseEntity.status(HttpStatus.OK).body(posts);
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

    @CrossOrigin("*")
    @GetMapping(value = POST_GET_BY_CATEGORY, produces = "application/json", params = {"category"})
    public ResponseEntity getPostsByCategory(@RequestParam("category") String category, HttpServletRequest request) {
        String code  = request.getHeader("Set-Cookie");
        if (loginPoolService.isUserAuthenticated(code)) {
            return ResponseEntity.status(HttpStatus.OK).body(this.postService.getPostsByCategory(category));
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }
}
