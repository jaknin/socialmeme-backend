package com.energyeet.socialmeme;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication
public class SocialMemeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SocialMemeApplication.class, args);
	}

}
