package com.energyeet.socialmeme.data.dto;

import java.util.List;

public class User {

    private String username;
    private long follower;
    private long follows;
    private String profilePicture;
    private List<Post> posts;

    public User(String username, long follower, long follows, String profilePicture, List<Post> posts) {
        this.username = username;
        this.follower = follower;
        this.follows = follows;
        this.profilePicture = profilePicture;
        this.posts = posts;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getFollower() {
        return follower;
    }

    public void setFollower(long follower) {
        this.follower = follower;
    }

    public long getFollows() {
        return follows;
    }

    public void setFollows(long follows) {
        this.follows = follows;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }
}
