package com.energyeet.socialmeme.data.dto;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * DTO Class for making a post
 */
public class Post implements Comparable<Post> {

    private int id;
    private String picture;
    private String userName;
    private int likes;
    private Timestamp timeStamp;
    private String heading;
    private String category;

    public Post() {}

    public Post(int id, String picture, String userName, int likes, Timestamp timeStamp, String heading, String category) {
        this.id = id;
        this.picture = picture;
        this.userName = userName;
        this.likes = likes;
        this.timeStamp = timeStamp;
        this.heading = heading;
        this.category = category;
    }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public Timestamp getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Timestamp timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public int compareTo(Post post) {
        if (this.timeStamp.after(post.timeStamp)) {
            return -1;
        } else if (post.timeStamp.after(this.timeStamp)) {
            return 1;
        } else {
            return 0;
        }
    }
}
