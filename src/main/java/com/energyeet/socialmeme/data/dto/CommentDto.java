package com.energyeet.socialmeme.data.dto;

/**
 * DTO Class for the comment entity
 */
public class CommentDto {

    private int postId;
    private String username;
    private String text;

    public CommentDto() {}

    public CommentDto(int postId, String username, String text) {
        this.postId = postId;
        this.username = username;
        this.text = text;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
