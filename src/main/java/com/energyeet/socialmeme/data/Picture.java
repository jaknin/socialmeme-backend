package com.energyeet.socialmeme.data;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Picture {
    private int pkId;
    private String picture;

    @Id
    @Column(name = "pk_id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getPkId() {
        return pkId;
    }

    public void setPkId(int pkId) {
        this.pkId = pkId;
    }

    @Basic
    @Column(name = "picture")
    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Picture picture1 = (Picture) o;
        return pkId == picture1.pkId &&
                Objects.equals(picture, picture1.picture);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pkId, picture);
    }
}
