package com.energyeet.socialmeme.data;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class User {
    private int pkId;
    private String lastName;
    private String firstName;
    private String userName;
    private String eMail;
    private String password;
    private Integer follower;
    private Integer fkPicture;

    @Id
    @Column(name = "pk_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getPkId() {
        return pkId;
    }

    public void setPkId(int pkId) {
        this.pkId = pkId;
    }

    @Basic
    @Column(name = "lastName")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "firstName")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "userName")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Basic
    @Column(name = "eMail")
    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "follower")
    public Integer getFollower() {
        return follower;
    }

    public void setFollower(Integer follower) {
        this.follower = follower;
    }

    @Basic
    @Column(name = "fk_picture")
    public Integer getFkPicture() { return fkPicture; }

    public void setFkPicture(Integer fkPicture) { this.fkPicture = fkPicture; }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return pkId == user.pkId &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(userName, user.userName) &&
                Objects.equals(eMail, user.eMail) &&
                Objects.equals(password, user.password) &&
                Objects.equals(follower, user.follower);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pkId, lastName, firstName, userName, eMail, password, follower);
    }
}
