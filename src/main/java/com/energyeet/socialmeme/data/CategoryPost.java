package com.energyeet.socialmeme.data;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "category_post", schema = "socialmeme", catalog = "")
public class CategoryPost {
    private int pkId;
    private Integer fkCategory;
    private Integer fkPost;

    @Id
    @Column(name = "pk_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getPkId() {
        return pkId;
    }

    public void setPkId(int pkId) {
        this.pkId = pkId;
    }

    @Basic
    @Column(name = "fk_category")
    public Integer getFkCategory() {
        return fkCategory;
    }

    public void setFkCategory(Integer fkCategory) {
        this.fkCategory = fkCategory;
    }

    @Basic
    @Column(name = "fk_post")
    public Integer getFkPost() {
        return fkPost;
    }

    public void setFkPost(Integer fkPost) {
        this.fkPost = fkPost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryPost that = (CategoryPost) o;
        return pkId == that.pkId &&
                Objects.equals(fkCategory, that.fkCategory) &&
                Objects.equals(fkPost, that.fkPost);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pkId, fkCategory, fkPost);
    }
}
