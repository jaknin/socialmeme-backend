package com.energyeet.socialmeme.data;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Follow {
    private int pkId;
    private Integer fkFollows;
    private Integer fkIsFollowed;

    @Id
    @Column(name = "pk_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getPkId() {
        return pkId;
    }

    public void setPkId(int pkId) {
        this.pkId = pkId;
    }

    @Basic
    @Column(name = "fk_follows")
    public Integer getFkFollows() {
        return fkFollows;
    }

    public void setFkFollows(Integer fkFollows) {
        this.fkFollows = fkFollows;
    }

    @Basic
    @Column(name = "fk_isFollowed")
    public Integer getFkIsFollowed() {
        return fkIsFollowed;
    }

    public void setFkIsFollowed(Integer fkIsFollowed) {
        this.fkIsFollowed = fkIsFollowed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Follow follow = (Follow) o;
        return pkId == follow.pkId &&
                Objects.equals(fkFollows, follow.fkFollows) &&
                Objects.equals(fkIsFollowed, follow.fkIsFollowed);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pkId, fkFollows, fkIsFollowed);
    }
}
