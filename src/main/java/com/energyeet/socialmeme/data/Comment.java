package com.energyeet.socialmeme.data;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Comment {
    private int pkId;
    private Integer fkPost;
    private Integer fkUser;
    private String comment;

    @Id
    @Column(name = "pk_id")
    public int getPkId() {
        return pkId;
    }

    public void setPkId(int pkId) {
        this.pkId = pkId;
    }

    @Basic
    @Column(name = "fk_post")
    public Integer getFkPost() {
        return fkPost;
    }

    public void setFkPost(Integer fkPost) {
        this.fkPost = fkPost;
    }

    @Basic
    @Column(name = "fk_user")
    public Integer getFkUser() {
        return fkUser;
    }

    public void setFkUser(Integer fkUser) {
        this.fkUser = fkUser;
    }

    @Basic
    @Column(name = "comment")
    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment1 = (Comment) o;
        return pkId == comment1.pkId &&
                Objects.equals(fkPost, comment1.fkPost) &&
                Objects.equals(fkUser, comment1.fkUser) &&
                Objects.equals(comment, comment1.comment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pkId, fkPost, fkUser, comment);
    }
}
