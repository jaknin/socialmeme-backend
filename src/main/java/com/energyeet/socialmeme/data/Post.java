package com.energyeet.socialmeme.data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class Post {
    private int pkId;
    private Integer fkUser;
    private Integer fkPicture;
    private Integer likes;
    private Timestamp timestamp;
    private String heading;

    @Id
    @Column(name = "pk_id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getPkId() {
        return pkId;
    }

    public void setPkId(int pkId) {
        this.pkId = pkId;
    }

    @Basic
    @Column(name = "fk_user")
    public Integer getFkUser() {
        return fkUser;
    }

    public void setFkUser(Integer fkUser) {
        this.fkUser = fkUser;
    }

    @Basic
    @Column(name = "fk_picture")
    public Integer getFkPicture() {
        return fkPicture;
    }

    public void setFkPicture(Integer fkPicture) {
        this.fkPicture = fkPicture;
    }

    @Basic
    @Column(name = "likes")
    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    @Basic
    @Column(name = "timestamp")
    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    @Basic
    @Column(name = "heading")
    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Post post = (Post) o;
        return pkId == post.pkId &&
                Objects.equals(fkUser, post.fkUser) &&
                Objects.equals(fkPicture, post.fkPicture) &&
                Objects.equals(likes, post.likes) &&
                Objects.equals(timestamp, post.timestamp) &&
                Objects.equals(heading, post.heading);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pkId, fkUser, fkPicture, likes, timestamp, heading);
    }
}
