package com.energyeet.socialmeme.service;

import com.energyeet.socialmeme.data.Category;
import com.energyeet.socialmeme.data.CategoryPost;
import com.energyeet.socialmeme.data.User;
import com.energyeet.socialmeme.data.dto.Post;
import com.energyeet.socialmeme.util.JpaHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Post Service
 */
@Service
public class PostService {

    @Autowired
    private JpaHelper jpaHelper;

    @Autowired
    private UserService userService;

    @Autowired
    private PictureService pictureService;

    /**
     * Add post to the database
     * @param post {@link Post} post to add
     */
    public void addPost(Post post) {
        //Get User by the username
        int userId = userService.findByUsername(post.getUserName()).getPkId();

        //Add the picture to the database
        int pictureId = pictureService.addPicture(post.getPicture());

        //Create the Post object
        com.energyeet.socialmeme.data.Post newPost = new com.energyeet.socialmeme.data.Post();
        newPost.setFkPicture(pictureId);
        newPost.setFkUser(userId);
        newPost.setHeading(post.getHeading());
        newPost.setLikes(post.getLikes());
        newPost.setTimestamp(post.getTimeStamp());
        jpaHelper.addPost(newPost);

        Category category = new Category();
        category.setCategory(post.getCategory());
        Category toCheck = this.jpaHelper.getCategoryByName(category);
        com.energyeet.socialmeme.data.Post post1 = jpaHelper.getPost(pictureId, userId);
        CategoryPost categoryPost = new CategoryPost();
        if (toCheck == null) {
            jpaHelper.addCategory(category);
            categoryPost.setFkPost(post1.getPkId());
            categoryPost.setFkCategory(category.getPkId());
            jpaHelper.addCategoryPost(categoryPost);
        } else {
            categoryPost.setFkPost(post1.getPkId());
            categoryPost.setFkCategory(toCheck.getPkId());
            jpaHelper.addCategoryPost(categoryPost);
        }
    }

    /**
     * Get all Posts from a user
     * @param username {@link String} username of the user
     * @return {@link List} of posts
     */
    public List<Post> getAllPostsFromUser(String username) {
        List<Post> posts = new ArrayList<>();
        User user = userService.findByUsername(username);
        List<com.energyeet.socialmeme.data.Post> data = jpaHelper.getAllPostsFromUser(user.getPkId());
        data.forEach(element -> {
            Post post = new Post(element.getPkId(), pictureService.getPictureById(element.getFkPicture()).getPicture(), user.getUserName(), element.getLikes(), element.getTimestamp(), element.getHeading(), "");
            posts.add(post);
        });
        return posts;
    }

    /**
     * Get all posts from followed users
     * @param users {@link List} of users
     * @return {@link List} of posts
     */
    public List<Post> getAllPostsFromFollowedUsers(List<User> users) {
        List<Post> posts = new ArrayList<>();
        users.forEach(user -> {
            posts.addAll(this.getAllPostsFromUser(user.getUserName()));
        });
        return posts;
    }
    /**
     * Like a post
     * @param post {@link Post} post to like
     */
    public void likePost(Post post) {
        jpaHelper.likePost(post);
    }

    /**
     * Delete post
     * @param post {@link Post} delete post
     * @return {@link Integer} amount of deletions
     */
    public int deletePost(com.energyeet.socialmeme.data.dto.Post post) {
        return jpaHelper.deletePost(post);
    }

    /**
     * Get Posts by heading
     * @param heading {@link String} heading
     * @return {@link List} of {@link Post}
     */
    public List<Post> getPostsByHeading(String heading) {
        return jpaHelper.getPostByHeading(heading);
    }

    /**
     * Get Posts by category
     * @param category {@link String}
     * @return {@link List} of {@link Post}
     */
    public List<Post> getPostsByCategory(String category) {
        return jpaHelper.getPostsByCategory(category);
    }
}
