package com.energyeet.socialmeme.service;

import com.energyeet.socialmeme.data.User;
import com.energyeet.socialmeme.util.Hasher;
import com.energyeet.socialmeme.util.JpaHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Service for the user handling
 */
@Service
public class UserService {

    @Autowired
    private JpaHelper jpaHelper;

    @Autowired
    private PictureService pictureService;

    /**
     * Save user to the database
     * @param user {@link User} user to save
     */
    public void save(User user) {
        user.setPassword(Hasher.hash(user.getPassword()));
        jpaHelper.save(user);
    }

    /**
     * Find user by username
     * @param username {@link String} username of the user
     * @return {@link User} user
     */
    public User findByUsername(String username) {
        return jpaHelper.findByUsername(username);
    }

    /**
     * Find User by username and password
     * @param username {@link String} username of the user
     * @param password {@link String} password of the user
     * @return {@link Boolean} if the user does exist
     */
    public boolean findByUserNameAndPassword(String username, String password) {
        return jpaHelper.findByUserNameAndPassword(username, Hasher.hash(password));
    }

    /**
     * Get amount of followers from a user
     * @param user {@link User}
     * @return {@link Long} amount of followers
     */
    public Long getAmountOfFollowers(User user) {
        return jpaHelper.getAmountOfFollowers(user);
    }

    /**
     * Get amount of followed users from user
     * @param user {@link User}
     * @return {@link Long} amount of followed users
     */
    public Long getAmountOfFollowed(User user) {
        return jpaHelper.getAmountOfFollowed(user);
    }

    /**
     * Get users by username
     * @param username {@link String} username of users to search for
     * @return {@link List} of {@link User}
     */
    public List<com.energyeet.socialmeme.data.dto.User> getUsersByName(String username) {
        List<User> users = this.jpaHelper.getUsersByName(username);
        List<com.energyeet.socialmeme.data.dto.User> newUsers = new ArrayList<>();
        users.forEach(element -> {
            com.energyeet.socialmeme.data.dto.User user = new com.energyeet.socialmeme.data.dto.User(element.getUserName(), 0, 0, this.pictureService.getPictureById(element.getFkPicture()).getPicture(), null);
            newUsers.add(user);
        });
        return newUsers;
    }

    /**
     * Update profile picture of user
     * @param pictureId {@link Integer}
     * @param user {@link User} user to update the profile picture from
     */
    public void updateProfilePicture(int pictureId, User user) {
        this.jpaHelper.updateProfilePicture(pictureId, user);
    }
}
