package com.energyeet.socialmeme.service;

import com.energyeet.socialmeme.data.Picture;
import com.energyeet.socialmeme.util.JpaHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Picture Service
 */
@Service
public class PictureService {

    @Autowired
    private JpaHelper jpaHelper;

    /**
     * Add a picture to the database
     * @param picture {@link String} picture encoded in Base64
     * @return {@link Integer} id of the picture
     */
    public int addPicture(String picture) {
        return jpaHelper.addPicture(picture);
    }

    /**
     * Get Picture by Id
     * @param pictureId {@link Integer} id of the picture
     * @return {@link Picture}
     */
    public Picture getPictureById(int pictureId) {
        return jpaHelper.getPictureById(pictureId);
    }

    /**
     * Get picture by its code
     * @param code {@link String} code of the picture
     * @return {@link Picture}
     */
    public Picture getPictureByCode(String code) {
        return this.jpaHelper.getPictureByCode(code);
    }

}
