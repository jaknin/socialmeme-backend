package com.energyeet.socialmeme.service;


import com.energyeet.socialmeme.data.Follow;
import com.energyeet.socialmeme.data.User;
import com.energyeet.socialmeme.util.JpaHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Service for following user matters
 */
@Service
public class FollowService {

    @Autowired
    private JpaHelper jpaHelper;

    /**
     * Follow a user by username
     * @param username {@link String} user that follows
     * @param username1 {@link String} user that is followed
     */
    public void followUser(String username, String username1) {
        int userFollows = jpaHelper.findByUsername(username).getPkId();
        int userToFollow = jpaHelper.findByUsername(username1).getPkId();
        jpaHelper.followUser(userFollows, userToFollow);
    }

    /**
     * Get all followed users from user
     * @param user {@link User} to get all followed user
     * @return {@link List} of all followed users
     */
    public List<User> getAllFollowedUsers(User user) {
        List<Follow> follows = jpaHelper.getAllFollowedUsersFromUser(user);
        List<User> users = new ArrayList<>();
        follows.forEach(follow -> {
            users.add(jpaHelper.findUserById(follow.getFkIsFollowed()));
        });
        return users;
    }
}
