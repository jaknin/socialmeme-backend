package com.energyeet.socialmeme.service;

import com.energyeet.socialmeme.data.Post;
import com.energyeet.socialmeme.util.JpaHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service for following comment matters
 */
@Service
public class CommentService {
    @Autowired
    private JpaHelper jpaHelper;

    /**
     * add a comment to a post
     * @param post {@link Integer} post id
     * @param username {@link String} username to search id
     * @param comment {@link String} the comment text
     */
    public void postComment(int post, String username, String comment) {
        int usersComment = jpaHelper.findByUsername(username).getPkId();
        jpaHelper.postComment(post, usersComment, comment);
    }
}
