package com.energyeet.socialmeme.service;

import com.energyeet.socialmeme.data.User;
import com.energyeet.socialmeme.util.LoginPool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service for interacting with the {@link com.energyeet.socialmeme.util.LoginPool}
 */
@Service
public class LoginPoolService {

    @Autowired
    private LoginPool loginPool;

    /**
     * Authenticates user
     * @param user {@link User} to authenticate
     */
    public String authenticateUser(User user) {
        return this.loginPool.insertUser(user);
    }

    /**
     * Check if user is authenticated
     * @param user {@link String} user to check
     * @return {@link boolean} if user is authenticated
     */
    public boolean isUserAuthenticated(String user) {
        return this.loginPool.checkUser(user);
    }

    /**
     * Deauthenticate user
     * @param user {@link User} user to deauthenticate
     */
    public void deauthenticateUser(User user) {
        this.loginPool.removeUser(user);
    }
}
