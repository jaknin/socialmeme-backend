package com.energyeet.socialmeme.service;

import com.energyeet.socialmeme.data.Category;
import com.energyeet.socialmeme.util.JpaHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service for categories
 */
@Service
public class CategoryService {

    @Autowired
    private JpaHelper jpaHelper;

    /**
     * Create a new category
     * @param category {@link Category} to create
     */
    public void createCategory(Category category) {
        jpaHelper.createCategory(category);
    }

    /**
     * Get all categories
     * @return {@link List} of {@link Category}
     */
    public List<Category> getAllCategories() {
        return jpaHelper.getAllCategories();
    }
}
