package com.energyeet.socialmeme.util;

import com.energyeet.socialmeme.data.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * {@link LoginPool} test
 */
public class LoginPoolTest {

    private LoginPool loginPool;

    @Before
    public void init() {
        loginPool = new LoginPool();
    }

    @Test
    public void insertUserTest() {
        String code = loginPool.insertUser(mockUser());
        Assert.assertEquals("Should be the same", Hasher.hash(mockUser().getUserName() + mockUser().getPassword()), code);
    }

    @Test
    public void checkUserTest() {
        String code = loginPool.insertUser(mockUser());
        Assert.assertTrue("Should be true", loginPool.checkUser(code));
    }

    @Test
    public void removeUserTest() {
        String code = loginPool.insertUser(mockUser());
        loginPool.removeUser(mockUser());
        Assert.assertFalse(loginPool.checkUser(code));
    }

    private User mockUser() {
        User user = new User();
        user.setUserName("SomeUsername");
        user.setPassword("SomePassword");
        return user;
    }
}
