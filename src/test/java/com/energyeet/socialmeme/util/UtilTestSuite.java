package com.energyeet.socialmeme.util;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Util test suite
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        HasherTest.class,
        JpaHelperTest.class,
        LoginPoolTest.class
})
public class UtilTestSuite {
}
