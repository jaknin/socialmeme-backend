package com.energyeet.socialmeme.util;

import org.junit.Assert;
import org.junit.Test;

/**
 * Hasher Test
 */
public class HasherTest {

    @Test
    public void hashTest() {
        Assert.assertEquals("532eaabd9574880dbf76b9b8cc00832c20a6ec113d682299550d7a6e0f345e25", Hasher.hash("Test"));
    }
}
