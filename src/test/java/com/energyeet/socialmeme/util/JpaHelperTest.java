package com.energyeet.socialmeme.util;

import com.energyeet.socialmeme.data.Category;
import com.energyeet.socialmeme.data.Comment;
import com.energyeet.socialmeme.data.Post;
import com.energyeet.socialmeme.data.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * {@link JpaHelper} test
 */
@RunWith(MockitoJUnitRunner.class)
public class JpaHelperTest {

    @Mock
    private JpaHelper jpaHelper;

    @Spy
    private JpaHelper dummyForConnectionTest;

    @Test
    public void saveUserTest() {
        User user = mockUser();
        Mockito.doNothing().when(jpaHelper).save(Mockito.any(User.class));
        jpaHelper.save(user);
        Mockito.verify(jpaHelper, Mockito.times(1)).save(Mockito.any(User.class));
    }

    @Test
    public void findByUsernameTest() {
        Mockito.doReturn(new User()).when(jpaHelper).findByUsername("SomeUsername");
        Assert.assertNotNull("Should no be null", jpaHelper.findByUsername("SomeUsername"));
    }

    @Test
    public void findByUsernameAndPasswordTest() {
        Mockito.doReturn(true).when(jpaHelper).findByUserNameAndPassword("SomeUsername", "SomePassword");
        Assert.assertTrue("Should not be true", jpaHelper.findByUserNameAndPassword("SomeUsername", "SomePassword"));
    }

    @Test
    public void findUserById() {
        Mockito.doReturn(new User()).when(jpaHelper).findUserById(1);
        Assert.assertNotNull("Should not be null", jpaHelper.findUserById(1));
    }

    @Test
    public void followUserTest() {
        jpaHelper.followUser(1, 2);
        Mockito.verify(jpaHelper, Mockito.times(1)).followUser(1, 2);
    }

    @Test
    public void addPictureTest() {
        Mockito.doReturn(1).when(jpaHelper).addPicture("AHDFJkfji321/Sddfdaf+");
        Assert.assertEquals("Should be 1", 1, jpaHelper.addPicture("AHDFJkfji321/Sddfdaf+"));
    }

    @Test
    public void addPost() {
        jpaHelper.addPost(new Post());
        Mockito.verify(jpaHelper, Mockito.times(1)).addPost(new Post());
    }

    @Test
    public void getAllPostsFromUserTest() {
        List<Post> posts = new ArrayList<>();
        posts.add(new Post());
        posts.add(new Post());
        Mockito.when(jpaHelper.getAllPostsFromUser(1)).thenReturn(posts);
        Assert.assertEquals("Should not be 2", 2, jpaHelper.getAllPostsFromUser(1).size());
    }

    @Test
    public void getPictureById() {
        jpaHelper.getPictureById(1);
        Mockito.verify(jpaHelper, Mockito.times(1)).getPictureById(1);
    }

    @Test
    public void postComment() {
        jpaHelper.postComment(1, 2, "CommentTest");
        Mockito.verify(jpaHelper, Mockito.times(1)).postComment(1, 2, "CommentTest");
    }

    @Test
    public void likePost() {
        com.energyeet.socialmeme.data.dto.Post post = new com.energyeet.socialmeme.data.dto.Post();
        jpaHelper.likePost(post);
        Mockito.verify(jpaHelper, Mockito.times(1)).likePost(post);
    }

    @Test
    public void createCategoryTest() {
        jpaHelper.createCategory(new Category());
        Mockito.verify(jpaHelper, Mockito.times(1)).createCategory(new Category());
    }

    @Test
    public void getAllCategoriesTest() {
        List<Category> list = new ArrayList<>();
        list.add(new Category());
        list.add(new Category());
        list.add(new Category());
        Mockito.doReturn(list).when(jpaHelper).getAllCategories();
        Assert.assertEquals("Should be 3", 3, jpaHelper.getAllCategories().size());
    }

    @Test
    public void deletePostTest() {
        com.energyeet.socialmeme.data.dto.Post post = new com.energyeet.socialmeme.data.dto.Post();
        Mockito.doReturn(1).when(jpaHelper).deletePost(post);
        jpaHelper.deletePost(new com.energyeet.socialmeme.data.dto.Post());
        Assert.assertEquals("Should be 1", 1, jpaHelper.deletePost(post));
    }

    @Test
    public void deleteCommentTest() {
        Mockito.doReturn(1).when(jpaHelper).deleteComment(1);
        Assert.assertEquals("Should be 1", 1, jpaHelper.deleteComment(1));
    }

    @Test
    public void connectionTest() {
        List<Category> list = dummyForConnectionTest.getAllCategories();
        Assert.assertTrue("Should be greater than 1", list.size() > 0);
    }

    private User mockUser() {
        User user = new User();
        user.setPkId(1);
        user.setUserName("SomeUsername");
        user.setPassword("SomePassword");
        user.setFollower(0);
        user.seteMail("SomeEmail");
        user.setFirstName("SomeFirstName");
        user.setLastName("SomeLastName");
        return user;
    }
}
