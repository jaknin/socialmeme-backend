package com.energyeet.socialmeme;

import com.energyeet.socialmeme.controller.ControllerTestSuite;
import com.energyeet.socialmeme.service.ServiceTestSuite;
import com.energyeet.socialmeme.util.UtilTestSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Test Suite for all the tests
 */
@RunWith(Suite.class)
@Suite.SuiteClasses( {
        ServiceTestSuite.class,
        UtilTestSuite.class,
        ControllerTestSuite.class
})
public class CompleteTestSuite {
}
