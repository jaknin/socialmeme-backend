package com.energyeet.socialmeme.controller;

import com.energyeet.socialmeme.service.CommentService;
import com.energyeet.socialmeme.service.LoginPoolService;
import com.energyeet.socialmeme.util.LoginPool;
import com.energyeet.socialmeme.util.Routes;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * {@link CommentController} test
 */
@RunWith(MockitoJUnitRunner.class)
public class CommentControllerTest implements Routes {

    @Mock
    private LoginPoolService loginPoolService;

    @Mock
    private CommentService commentService;

    @Mock
    private LoginPool loginPool;

    @InjectMocks
    private CommentController commentController;

    private MockMvc mockMvc;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(commentController).build();
    }

    @Test
    public void addCommentTest() throws Exception {
        mockMvc.perform(
            MockMvcRequestBuilders.post(COMMENT_ROUTE + COMMENT_ADD_ROUTE)
                .contentType("application/json")
                .content(
                    "{\n" +
                    "    \"postId\": 1,\n" +
                    "    \"username\": \"SomeUsername\",\n" +
                    "    \"text\": \"CommentTest\"\n" +
                    "}"
                )
        ).andExpect(MockMvcResultMatchers.status().isBadRequest());
    }
}
