package com.energyeet.socialmeme.controller;

import com.energyeet.socialmeme.service.CategoryService;
import com.energyeet.socialmeme.service.LoginPoolService;
import com.energyeet.socialmeme.util.JpaHelper;
import com.energyeet.socialmeme.util.LoginPool;
import com.energyeet.socialmeme.util.Routes;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * {@link CategoryController} test
 */
@RunWith(MockitoJUnitRunner.class)
public class CategoryControllerTest implements Routes {

    @Mock
    private LoginPoolService loginPoolService;

    @Mock
    private LoginPool loginPool;

    @Mock
    private JpaHelper jpaHelper;

    @Mock
    private CategoryService categoryService;

    @InjectMocks
    private CategoryController categoryController;

    private MockMvc mockMvc;

    public CategoryControllerTest() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(categoryController).build();
    }

    @Test
    public void createCategoryTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(CATEGORY_ROUTE + CATEGORY_CREATE_ROUTE)
            .contentType("application/json")
            .content("{\n" +
                    "\t\"pkId\": 0,\n" +
                    "\t\"category\": \"DankMemes\"\n" +
                    "}")
        ).andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getAllCategoriesTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(CATEGORY_ROUTE + CATEGORY_GET_ALL_ROUTE)
            .accept("application/json")
        ).andExpect(MockMvcResultMatchers.status().isForbidden());
    }
}
