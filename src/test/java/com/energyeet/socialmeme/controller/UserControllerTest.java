package com.energyeet.socialmeme.controller;

import com.energyeet.socialmeme.data.User;
import com.energyeet.socialmeme.service.LoginPoolService;
import com.energyeet.socialmeme.service.UserService;
import com.energyeet.socialmeme.util.LoginPool;
import com.energyeet.socialmeme.util.Routes;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * {@link UserController} test
 */
@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest implements Routes {

    @Mock
    private UserService userService;

    @Mock
    private LoginPoolService loginPoolService;

    @Mock
    private LoginPool loginPool;

    @InjectMocks
    private UserController userController;

    private MockMvc mockMvc;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    @Test
    public void registrationTest() throws Exception {
        mockMvc.perform(
            MockMvcRequestBuilders.post(USER_ROUTE + USER_REGISTER_ROUTE)
                .contentType("application/json")
                .content("{\n" +
                        "\t\"lastName\": \"Jakob\",\n" +
                        "\t\"firstName\": \"Danilo\",\n" +
                        "\t\"userName\": \"Churrer\",\n" +
                        "\t\"eMail\": \"danilo.murer@gmail.com\",\n" +
                        "\t\"password\": \"danilo\",\n" +
                        "\t\"follower\": 0\n" +
                        "}")
                .accept("application/json")
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void loginTest() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post(USER_ROUTE + USER_LOGIN_ROUTE)
                        .contentType("application/json")
                        .content("{\n" +
                                "\t\"lastName\": \"Jakob\",\n" +
                                "\t\"firstName\": \"Danilo\",\n" +
                                "\t\"userName\": \"Churrer\",\n" +
                                "\t\"eMail\": \"danilo.murer@gmail.com\",\n" +
                                "\t\"password\": \"danilo\",\n" +
                                "\t\"follower\": 0\n" +
                                "}")
        ).andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void logoutTest() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.post(USER_ROUTE + USER_LOGOUT_ROUTE)
                        .contentType("application/json")
                        .content("{\n" +
                                "\t\"lastName\": \"Jakob\",\n" +
                                "\t\"firstName\": \"Danilo\",\n" +
                                "\t\"userName\": \"Churrer\",\n" +
                                "\t\"eMail\": \"danilo.murer@gmail.com\",\n" +
                                "\t\"password\": \"danilo\",\n" +
                                "\t\"follower\": 0\n" +
                                "}")
                        .accept("application/json")
        ).andExpect(MockMvcResultMatchers.status().isOk());
    }
}
