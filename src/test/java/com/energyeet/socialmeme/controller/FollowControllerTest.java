package com.energyeet.socialmeme.controller;

import com.energyeet.socialmeme.service.FollowService;
import com.energyeet.socialmeme.service.LoginPoolService;
import com.energyeet.socialmeme.util.LoginPool;
import com.energyeet.socialmeme.util.Routes;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * {@link FollowController} test
 */
@RunWith(MockitoJUnitRunner.class)
public class FollowControllerTest implements Routes {

    @Mock
    private FollowService followService;

    @Mock
    private LoginPoolService loginPoolService;

    @Mock
    private LoginPool loginPool;

    @InjectMocks
    private FollowController followController;

    private MockMvc mockMvc;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(followController).build();
    }

    @Test
    public void followUserTest() throws Exception {
        mockMvc.perform(
                MockMvcRequestBuilders.get(FOLLOW_ROUTE + FOLLOW_USER_ROUTE)
                    .param("username", "SomeUsername")
                    .param("username1", "SomeOtherUsername")
        ).andExpect(MockMvcResultMatchers.status().isForbidden());
    }
}
