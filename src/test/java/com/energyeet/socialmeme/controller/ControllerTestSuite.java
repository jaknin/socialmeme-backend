package com.energyeet.socialmeme.controller;

import com.energyeet.socialmeme.data.Comment;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Controller test suite
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        UserControllerTest.class,
        FollowControllerTest.class,
        CommentControllerTest.class,
        PostControllerTest.class,
        CategoryControllerTest.class
})
public class ControllerTestSuite {
}
