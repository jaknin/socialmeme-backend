package com.energyeet.socialmeme.controller;

import com.energyeet.socialmeme.service.LoginPoolService;
import com.energyeet.socialmeme.service.PostService;
import com.energyeet.socialmeme.util.LoginPool;
import com.energyeet.socialmeme.util.Routes;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * {@link PostController} test
 */
@RunWith(MockitoJUnitRunner.class)
public class PostControllerTest implements Routes {

    @Mock
    private LoginPoolService loginPoolService;

    @Mock
    private LoginPool loginPool;

    @Mock
    private PostService postService;

    @InjectMocks
    private PostController postController;

    private MockMvc mockMvc;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(postController).build();
    }

    @Test
    public void createPostTest() throws Exception {
        mockMvc.perform(
            MockMvcRequestBuilders.post(POST_ROUTE + POST_CREATE_ROUTE)
                .contentType("application/json")
                .content(
                    "{\n" +
                        "\t\"picture\": \"fdjfdKJfkoadf+/fdjljfkdl\"\n" +
                        "\t\"userName\": \"Churrer\",\n" +
                        "\t\"timeStamp\": \"2020-01-05T18:25:43.511Z\",\n" +
                        "\t\"heading\": \"Steinbock\"\n" +
                    "}"
                )
        ).andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void getAllPostsFromUserTest() throws Exception {
        mockMvc.perform(
            MockMvcRequestBuilders.get(POST_ROUTE + POST_GET_ALL_USER_POST_ROUTE)
                .param("username", "SomeUsername")
        ).andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void likePost() throws Exception {
        mockMvc.perform(
            MockMvcRequestBuilders.post(POST_ROUTE + POST_LIKE_ROUTE)
                .contentType("application/json")
                .content(
                    "{\n" +
                        "\t\"id\": 4,\n" +
                        "\t\"picture\": \"fdjfdKJfkoadf+/fdjljfkdl\",\n" +
                        "\t\"userName\": \"Churrer\",\n" +
                        "\t\"timeStamp\": \"2020-01-05T18:25:43.511Z\",\n" +
                        "\t\"heading\": \"Steinbock\"\n" +
                    "}")
        ).andExpect(MockMvcResultMatchers.status().isForbidden());
    }
}
