package com.energyeet.socialmeme.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * ServiceTestSuite
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        LoginPoolServiceTest.class,
        PictureServiceTest.class,
        UserServiceTest.class,
        FollowServiceTest.class,
        CommentServiceTest.class,
        CategoryServiceTest.class
})
public class ServiceTestSuite {
}
