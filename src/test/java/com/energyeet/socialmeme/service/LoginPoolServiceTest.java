package com.energyeet.socialmeme.service;

import com.energyeet.socialmeme.data.User;
import com.energyeet.socialmeme.util.Hasher;
import com.energyeet.socialmeme.util.LoginPool;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * {@link LoginPoolService} test
 */
@RunWith(MockitoJUnitRunner.class)
public class LoginPoolServiceTest {

    @Mock
    private LoginPool loginPool;

    @InjectMocks
    private LoginPoolService loginPoolService;

    @Test
    public void whenUserDoesNotExistShouldReturnFalse() {
        User user = new User();
        user.setPassword("TestPassword");
        user.setUserName("TestUsername");

        Assert.assertFalse(loginPoolService.isUserAuthenticated(Hasher.hash(user.getUserName() + user.getPassword())));
    }

    @Test
    public void whenUserDoesExistShouldReturnTrue() {
        User user = new User();
        user.setPassword("TestPassword");
        user.setUserName("TestUsername");

        //Log-In User
        loginPoolService.authenticateUser(user);

        Assert.assertFalse(loginPoolService.isUserAuthenticated(Hasher.hash(user.getUserName() + user.getPassword())));
    }

    @Test
    public void whenUserIsLoggedOutShouldReturnFalse() {
        User user = new User();
        user.setPassword("TestPassword");
        user.setUserName("TestUsername");

        //Log-In User
        loginPoolService.authenticateUser(user);
        //Log-Out User
        loginPoolService.deauthenticateUser(user);

        Assert.assertFalse(loginPoolService.isUserAuthenticated(Hasher.hash(user.getUserName() + user.getPassword())));
    }
}
