package com.energyeet.socialmeme.service;

import com.energyeet.socialmeme.data.Picture;
import com.energyeet.socialmeme.util.JpaHelper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

/**
 * {@link PictureService} test
 */
@RunWith(MockitoJUnitRunner.class)
@DataJpaTest
public class PictureServiceTest {

    @Mock
    private JpaHelper jpaHelper;

    @InjectMocks
    private PictureService pictureService;

    @Test
    public void whenExistingPictureIsSearchedShouldNotBeNull() {
        Mockito.when(pictureService.getPictureById(1)).thenReturn(new Picture()); // Picture with id 1 always exist
        Assert.assertNotNull(pictureService.getPictureById(1));
    }

    @Test
    public void whenPictureNotExistShouldReturnNull() {
        Mockito.when(pictureService.getPictureById(0)).thenReturn(null); // Picture with id 0 never exists
        Assert.assertNull(pictureService.getPictureById(0));
    }
}
