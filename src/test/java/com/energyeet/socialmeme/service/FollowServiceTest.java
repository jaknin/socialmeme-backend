package com.energyeet.socialmeme.service;

import com.energyeet.socialmeme.data.User;
import com.energyeet.socialmeme.util.JpaHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * {@link FollowService} test
 */
@RunWith(MockitoJUnitRunner.class)
public class FollowServiceTest {

    @Mock
    private JpaHelper jpaHelper;

    @InjectMocks
    private FollowService followService;

    @Test
    public void followUserTest() {
        Mockito.doReturn(new User()).when(jpaHelper).findByUsername("SomeUsername");
        Mockito.doReturn(new User()).when(jpaHelper).findByUsername("SomeSecondUsername");
        Mockito.doNothing().when(jpaHelper).followUser(0, 0);

        followService.followUser("SomeUsername", "SomeSecondUsername");
    }

    /**
     * This Test should throw a {@link NullPointerException}
     */
    @Test(expected = NullPointerException.class)
    public void followNotExistingUserTest() {
        Mockito.doReturn(new User()).when(jpaHelper).findByUsername("SomeUsername");

        followService.followUser("SomeUsername", "SomeNonExistingUsername");
    }
}
