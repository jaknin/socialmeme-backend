package com.energyeet.socialmeme.service;

import com.energyeet.socialmeme.data.Category;
import com.energyeet.socialmeme.util.JpaHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * {@link CategoryService} test
 */
@RunWith(MockitoJUnitRunner.class)
public class CategoryServiceTest {

    @Mock
    private JpaHelper jpaHelper;

    @InjectMocks
    private CategoryService categoryService;

    @Test
    public void createCategoryTest() {
        Category category = new Category();
        categoryService.createCategory(category);
        Mockito.verify(jpaHelper, Mockito.times(1)).createCategory(category);
    }

    @Test
    public void getAllCategoriesTest() {
        categoryService.getAllCategories();
        Mockito.verify(jpaHelper, Mockito.times(1)).getAllCategories();
    }
}
