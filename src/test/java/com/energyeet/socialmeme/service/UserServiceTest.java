package com.energyeet.socialmeme.service;

import com.energyeet.socialmeme.data.User;
import com.energyeet.socialmeme.util.JpaHelper;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * {@link UserService} test
 */
@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    private JpaHelper jpaHelper;

    @InjectMocks
    private UserService userService;

    @Test
    public void findUserByUsernameTest() {
        Mockito.when(userService.findByUsername("SomeUsername")).thenReturn(new User());
        //Should return not null
        Assert.assertNotNull("Shouldn't be null", userService.findByUsername("SomeUsername"));
        //Should return null
        Assert.assertNull("Should be null", userService.findByUsername("SomeOtherUsername"));
    }

    @Test
    public void findByUsernameAndPasswordTest() {
        Mockito.when(userService.findByUserNameAndPassword("SomeUsername", "SomePassword")).thenReturn(true);
        //Should return true
        Assert.assertTrue("Should be true", userService.findByUserNameAndPassword("SomeUsername", "SomePassword"));
        //Should return false
        Assert.assertFalse("Should be false", userService.findByUserNameAndPassword("SomeOtherUsername", "SomeOtherPassword"));
    }

}
