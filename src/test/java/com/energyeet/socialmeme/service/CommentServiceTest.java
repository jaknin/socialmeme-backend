package com.energyeet.socialmeme.service;

import com.energyeet.socialmeme.data.User;
import com.energyeet.socialmeme.util.JpaHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * {@link CommentService} test
 */
@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {

    @Mock
    private JpaHelper jpaHelper;

    @InjectMocks
    private CommentService commentService;

    @Test
    public void postCommentTest() {
        Mockito.doReturn(new User()).when(jpaHelper).findByUsername("SomeUsername");
        commentService.postComment(1, "SomeUsername", "CommentTest");
    }
}
